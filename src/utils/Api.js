import axios from 'axios'

axios.defaults.baseURL = process.env.API_URL
axios.defaults.timeout = 60000
axios.defaults.headers.common['Content-Type'] = 'application/json'
// fix necesary, or not include header Content-Type in request type GET
axios.defaults.data = {}

class Api {
  constructor() {
    let service = axios.create()
    service.interceptors.response.use(this.handleSuccess, this.handleError)
    this.service = service
  }

  handleSuccess(response) {
    console.log('handleSuccess', response)
    return response
  }

  handleError = (error) => {
    switch (error.response.status) {
      case 401:
        this.redirectTo(document, '/')
        break
      case 404:
        this.redirectTo(document, '/404')
        break
      default:
        this.redirectTo(document, '/500')
        break
    }
    return Promise.reject(error)
  }

  redirectTo = (document, path) => {
    document.location = path
  }
  
  get(path, callback) {
    return this.service.get(path).then(
      (response) => callback(response.status, response.data)
    );
  }

  patch(path, payload, callback) {
    return this.service.request({
      method: 'PATCH',
      url: path,
      responseType: 'json',
      data: payload
    }).then((response) => callback(response.status, response.data));
  }

  post(path, payload, callback) {
    return this.service.request({
      method: 'POST',
      url: path,
      responseType: 'json',
      data: payload
    }).then((response) => callback(response.status, response.data));
  }
}

export default new Api
