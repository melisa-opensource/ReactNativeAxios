import React from 'react'
import {
	StatusBar,
	SafeAreaView,
	ScrollView,
	Text,
	StyleSheet,
	View
} from 'react-native'
import {
	Colors
} from 'react-native/Libraries/NewAppScreen'

const App = () => {
	return (
		<>
			<StatusBar barStyle="dark-content" />
			<SafeAreaView>
				<ScrollView
					contentInsetAdjustmentBehavior="automatic"
					style={styles.scrollView}
				>
					<View style={styles.body}>
						<Text>Hello Axios</Text>
					</View>
				</ScrollView>
			</SafeAreaView>
		</>
	)
}

const styles = StyleSheet.create({
	scrollView: {
		backgroundColor: Colors.lighter,
	},
	body: {
		backgroundColor: Colors.white,
	}
})

export default App
